const people = [
    {
        name: 'Arisa',
        department: 'BP',
        gender: 'F'
    },
    {
        name: 'Ham',
        department: 'IT',
        gender: 'F'
    },
    {
        name: 'Alice',
        department: 'IT',
        gender: 'F'
    },
    {
        name: 'Anna',
        department: 'DA',
        gender: 'F'
    },
    {
        name: 'Larry',
        department: 'Sales',
        gender: 'M'
    },
    {
        name: 'Ria',
        department: 'Sales',
        gender: 'F'
    },
    {
        name: 'JD',
        department: 'Sales',
        gender: 'M'
    },
    {
        name: 'Thor',
        department: 'Sales',
        gender: 'M'
    },
    {
        name: 'Karl',
        department: 'Sales',
        gender: 'M'
    },
    {
        name: 'Rachel',
        department: 'Sales',
        gender: 'F'
    }
];


function listByGender(gender){
    var _people = [];
    people.map(x=>{
        if(x.gender.toLowerCase() == gender.toLowerCase())
            _people.push(x.name);
    });
    return _people;
}

function groupByDepartment() {
    var _grouped = {};
    people.map(x=>{
        if(!_grouped[x.department]){
            _grouped[x.department] = [];
        }
        _grouped[x.department].push(x.name);
    });

    //another option that output an array instead of an object.
    /*
    var _option2 = [];
    var _grouped2 = Object.keys(_grouped);
    _grouped2.forEach(function(val,i){
        var _format = {
            department: val,
            people: _grouped[val]
        }
        _option2.push(_format);
    })
    _grouped = _option2[];
    */

    return _grouped;
}